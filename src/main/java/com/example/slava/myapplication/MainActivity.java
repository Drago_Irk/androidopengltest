package com.example.slava.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button btnShowOpenGL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnShowOpenGL = (Button) findViewById(R.id.btnShowOpenGL);
        btnShowOpenGL.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnShowOpenGL:
                btnShowOpenGL.setText("Нажата кнопка");
                Intent intent = new Intent(this, OpenGLActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }
}
