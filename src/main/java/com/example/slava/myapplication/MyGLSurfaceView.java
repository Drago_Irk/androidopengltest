package com.example.slava.myapplication;

import android.content.Context;
import android.opengl.GLSurfaceView;

/**
 * Created by Slava on 08.07.2016.
 */
class MyGLSurfaceView extends GLSurfaceView {

    private final MyGLRenderer mRenderer;

    public MyGLSurfaceView(Context context){
        super(context);

        setEGLContextClientVersion(2);

        mRenderer = new MyGLRenderer();

        setRenderer(mRenderer);
    }
}
